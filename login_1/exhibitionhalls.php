<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Amity gallery final render Edit Reflections.jpg">
          
            <a href="assets/img/Poster Day Wise/3.pdf" id="exhVideo1" class="showpdf exhVideo1" data-vidid="11345" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/7.pdf" id="exhVideo9" class="showpdf exhVideo9" data-vidid="8">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/8.pdf" id="exhVideo11" class="showpdf exhVideo11" data-vidid="10">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/23.pdf" id="exhVideo2" class="showpdf exhVideo2" data-vidid="1" >
                <div class="indicator d-6"></div>
            </a>
             <a href="assets/img/Poster Day Wise/24.pdf" id="exhVideo3" class="showpdf exhVideo3" data-vidid="2">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/26.pdf" id="exhVideo10" class="showpdf exhVideo10" data-vidid="9" >
                <div class="indicator d-6"></div>
            </a>  
            <a href="assets/img/Poster Day Wise/30.pdf" id="exhVideo7" class="showpdf exhVideo7" data-vidid="6">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/52.pdf" id="exhVideo8" class="showpdf exhVideo8" data-vidid="7">
                <div class="indicator d-6"></div>
            </a>         
            <!-- <a href="assets/resources/2.Sample Posters - KB_RGM_new grant  -  Compatibility Mode.pdf" id="exhVideo" class="showpdf"></a> -->
            <a href="assets/img/Poster Day Wise/58.pdf" id="exhVideo" class="showpdf exhVideo5" data-vidid="11">
                <div class="indicator d-6"></div>
            </a>
            <a href="bondk.php" id="bondk" class="bondk" data-vidid="21">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/62.pdf" id="exhVideo6" class="showpdf exhVideo6" data-vidid="5">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/74.pdf" id="exhVideo5" class="showpdf exhVideo5" data-vidid="4">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Poster Day Wise/79.pdf" id="exhVideo4" class="showpdf exhVideo4" data-vidid="3">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="colsmartA.php" id="colsmartA">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="lizolid.php" id="lizolid">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="stiloz.php" id="stiloz">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamp.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <div id="next-button">
                <a href="posterseession.php"><i class="fas fa-arrow-alt-circle-right"></i> Next</a>
            </div> -->
        </div>
     
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<script>
    $(function() {
        $('.exhVideo1').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo2').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo3').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo4').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });

    $(function() {
        $('.exhVideo5').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo6').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo7').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo8').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });

    $(function() {
        $('.exhVideo9').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo10').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo11').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.bondk').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>