<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = '913234bfe40b6433e81ceb7573bdd9b9c069ad2c08d89d3beb33bdc351ed5954';
require_once "exhibcheck.php";
$curr_room = 'bondk';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<style>
table td, table tr, table th{
background: transparent !important;
}

.txtcolor{
    color:black;
}

table.dataTable thead .sorting_asc {
    background-image: url("") !important;
}

.backbtn{
    float:left;
   background-color: black;
   padding:3px;
}

.h2{
    color:black
}
/* .border{
    border-color:black;
} */
.bg-image{
    /* background-image: url('assets/img/listbg.jpg');
    background-size: cover;   */
    background-color:default;
    font-size: 15px;
    
}
.mfp-iframe-holder .mfp-content {
    line-height: 0;
    width: 100%;
    max-width: 1100px;
}
tr:nth-child(even) {
            background-color: lightgrey !important;
        };
th {
  text-align: left;
}
</style>
<body>
<div class="page-content">
    <div id="content1">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
       
        <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white">
        <div id="back-button" class="backbtn">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
        </div>
        <h1 class="mr-4 text-dark">POSTER LISTS</h1>
        <a href="#" class="btn btn-info" role="button" aria-pressed="true">18.12.2021</a>
        <a href="#example" class="btn btn-info" role="button" aria-pressed="true">19.12.2021</a>
       
        <table id="example" class="table" style="width:100%;">
        <thead>
            <tr>
                <th>S No.</th>
                <th>Subject</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Poster</th>
                <th>Breakout Room</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>80</td>
                <td>Ectodysplasin missense mutation affecting the EDA <br>Furin cleavage site are the predominant cause of X <br> linked hypohidrotic ectodermal dysplasia in India</td>
                <td>Ajay Kumar Chaudhary</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/80.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTNlY2FkZjMtYTY1Ny00NGEzLThmZjItODViOTZmYTE4Yjg0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>81</td>
                <td>Molecular analysis of the functional role of co <br>activator binding protein PIMT in macrophage mediated<br> insulin resistance</td>
                <td>Naga Lakshmi Challa</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/81.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTg0MDQ3NjYtMmZjMi00YWFmLWFkMzMtOTM4NjI3OGVjN2E2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>82</td>
                <td>Peptide-Based Wound Care for Faster Blood-Clotting <br>and Diabetic Wound Healing</td>
                <td>Paramita Gayen</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/82.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjI1OTkwMDUtOWI4YS00Y2U3LTg3N2EtYWZiMmMzNDY2Mjdh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>83</td>
                <td>Binding of anionic surfactants with native and <br>partially-unfolded ribonuclease A: the role of<br> hydrophobic interaction</td>
                <td>Sanjay Kumar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/83.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGM0ZGU2YjMtODdmZS00YzRjLThiOTctNDZlZTM2MjAwOGNl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>84</td>
                <td>Role of PIMT a transcriptional regulator in <br>pancreatic beta cells</td>
                <td>Rahul Sharma</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/84.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGJjMzY5MzEtYTgyYy00MDYwLTk3ODMtYTQzNDAzM2E0YmIz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>85</td>
                <td>PRIP-interacting protein with methyl transferase <br>domain (PIMT) regulates lipid metabolism<br> and adipogenesis</td>
                <td>Rebecca Kristina Edwin</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/85.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWZhNjRmNzctZjVmMi00Mjg0LTliYzAtZTIwNTQyOGJhYjc5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>86</td>
                <td>Copper(II)-Catalyzed Decarboxylative Cyclization <br>for Accessing Biologically Relevant 3-(2-Furanyl<br>) Indoles via 3-Cyanoacetyl Indoles and <br>Cinnamic Acids</td>
                <td>Sarita Katiyar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/86.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWFkMDA1YzgtYjY5YS00YmVlLTg1OWItZmJhM2FlYWM1NmZl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>87</td>
                <td>PHLPP1 promotes PINK1-mediated mitophagy during <br>mitochondrial stress</td>
                <td>Kanika Chandra</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/87.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmQzZjNiNmUtYjNlZC00NmZjLWE5MzktMDQ4ZjU0ZDI5Y2Rk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>88</td>
                <td>Structure function aspect of PAS domain containing <br>phosphoglycerate kinase from Leishmania major</td>
                <td>Saroj Biswas</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/88.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDg3NWQ0M2MtMWI2ZC00MTBmLWI2OTMtODJhZmE5OWVkOGVj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>89</td>
                <td>Functional characterization of novel LmChac1 and <br>LmChac2 protein of Leishmania major</td>
                <td>Sumit Das</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/89.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZjZlNzNmNzgtNTQzMC00YjVmLTgzOGUtNzI0N2VlOTdiODQw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>90</td>
                <td>Effect of charged amino acids on the α-synuclein fibril <br>formation: spectroscopic and computational <br>analysis</td>
                <td>Archi Saurabh</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/90.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Y2JhZmM2ZDMtNGY3Ni00N2FkLWEwMDItMjY0MDcwZDYxMDNl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>91</td>
                <td>Exploring the role of inorganic polyphosphate in<br> mitochondrial physiology</td>
                <td>Jayashree Ladke</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/91.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODAwM2VmNDMtZTI1NS00Zjk2LWE1MGMtMjFhM2UyYzU4MTc4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>92</td>
                <td>Design of transcription repressor peptides from a <br>bacteriophage capsid protein</td>
                <td>Pankaj V. Sharma</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/92.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmMzYWJjMGItZjJhYS00MDI2LTkwNGYtNGEwOWQ3MjFhZmI0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>93</td>
                <td>Comparative study of effect of ionic liquids with <br>alicyclic and aromatic heterocyclic cations <br>on the fibrillation and stability of Lysozyme</td>
                <td>Pratibha Kushawaha</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/93.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWM0M2I4ZjItN2NjOC00NWNmLTkzMzktNzlkZjM3NTQxZWQ2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>94</td>
                <td>Glyceraldehyde-3-phosphate dehydrogenase present <br>in extracellular vesicles from Leishmania<br> major suppresses host TNF-alpha expression</td>
                <td>Priya Das</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/94.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODJhMzA5NTktZDI2NC00ZTljLWI1YTAtMmQ1MjkyZTVjNjU5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>95</td>
                <td>The potential of diatomaceous earth as highly efficient<br> drug delivery system</td>
                <td>Abhishek Saxena</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/95.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NTNiMjAyYWUtYWI5ZS00MzE3LTk2NTEtNGJiYjgxOTRiZjg3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>96</td>
                <td>Amino Acid-Catalyzed Direct Synthesis of β-Keto Sulfones <br>via Aerobic Difunctionalization of <br>Terminal Alkynes in an Aqueous Medium</td>
                <td>Deepak Bhadoria</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/96.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGJlOThkYzctODA4ZC00NGFhLTlmOWEtNWUzNjZkNmE0M2Iy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>97</td>
                <td>Mutational analysis of DhNik1, a HHK3 ortholog, reveals that <br>HAMP domain regulates multiple signalling <br>output conformations of protein</td>
                <td>Soorya Partap Sasan</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/97.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWRlMGM5NDAtZmRmOC00NThiLWFjNWUtNTBiMjU4OTg5NTBl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>98</td>
                <td>Rabaptin5 acts as a Guanine nucleotide exchange factor<br>s (GEF) of Rab7l1 and is involved <br>sin phagosome-lysosome fusion</td>
                <td>Rohini Shrivastava</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/98.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmRlMzJiMzMtZDJkYS00M2I4LWJlZjctYzdhMzRkMjdhZGYy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>99</td>
                <td>The secretory protein PPE2 can globally affect <br>macrophage immune functions</td>
                <td>Manoj Kumar Bisht</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/99.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjViMTlhYjYtYmMwZi00NzJlLWI2ZjUtNzM5NjU1ZjJlYjAz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>100</td>
                <td>Effect of mineralocorticoid along with glycation on <br>structural and functional modifications<br> of albumin and NRF – 2 signalling<br> pathway in renal cells</td>
                <td>Deepesh Gaikwad</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/100.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTRmYTVhODAtZTZhNy00M2MzLTg1ZmItZGI4OTE2YjJmOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>101</td>
                <td>Aldosterone detoriorates renal cells via interaction <br>with AGEs-RAGE/NF-κB/Rac-1 pathway in<br> diabetic nephropathy</td>
                <td>Mayura Apte</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/101.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODk1MDA5NzQtODlmNi00NDU1LWJhZDQtYzhlNzBkMmU4ZGU1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>102</td>
                <td>Nano-formulation of naturally occurring pentacyclic <br>triterpenoid ameliorates changes in bone <br>metabolism and body composition associated<br> with ovariectomized rats</td>
                <td>Anirban Sardar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/102.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Y2Q3ZmViOGEtYjBmMy00YmJmLTgzMTUtNDgzODllOTAzMWUy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>103</td>
                <td>Role of Post-translational modifications of multifunctional <br>Ku protein in cell cycle progression</td>
                <td>Indranil Modak</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/103.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDY3MWI2OTAtMTljNi00MDc1LThmZmItNTAyYjhhZDEyYTI3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>104</td>
                <td>Hydrogel-mediated Delivery of Steroids can Alleviate <br>Psoriasis via Altering the Th17 and γδ Cells</td>
                <td>Kajal Rana</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/104.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzY0YjIzNTAtODE3MS00NzMzLTk0N2EtMDY3MDFlOWZhYzgz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>105</td>
                <td>TIP60-PXR complex induces early wound healing by upregulating<br> expression of CDC42 and ROCK1 genes</td>
                <td>Shraddha Dubey</td>         
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/105.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YmFhNTAzNmEtMmY0Mi00YzFmLWFlNzAtYTQzMmRlNjFhYjdi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>106</td>
                <td>Base promoted peroxide systems for the efficient synthesis<br> of nitroarenes and benzamides</td>
                <td>Alisha Ansari</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/106.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDgzOGFhNWYtYTVkOS00YWY3LTg1ZTUtMjg1NTM5NWM2ZmFm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>107</td>
                <td>Leishmania cleaves iron chaperones to intervene iron loading<br> into ferritin in host macrophages</td>
                <td>Sameeksha Yadav</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/107.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzAwM2RiZmItZmM1ZC00MDJmLThkMzAtODc5NWNkMDg1MjA5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>108</td>
                <td>Over-expression of serine o-acetyltransferase confers survival<br> advantage and resistance to drug pressure in<br> Leishmania donovani</td>
                <td>Afreen</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/108.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGQ2NmZhN2MtMWFlNS00NjRiLWFiOGMtOGE3NzE5MWQwOGFj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>109</td>
                <td>Characterization of dithiol glutaredoxin protein <br>of Leishmania donovani and its up-regulation in <br>Amphotericin B resistant parasites</td>
                <td>Bhawna Priya</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/109.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODZiNTYwYmMtNzIyZC00MTI4LTljOTYtZGJmZDUyMmY5MWRj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>110</td>
                <td>Proteomic analysis reveals novel Amphotericin B <br>resistant and stage specific secreted proteins<br> in Leishmania donovani</td>
                <td>Vahab Ali</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/110.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDNjYjcxNzItMWJkYS00YzJiLWI0NjAtYzlhMDI0YWNjNWFk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>111</td>
                <td>Identification, characterisation and interaction of <br>Cfd1-Nbp35 proteins in L. donovani reveals contributory<br> role for Amphotericin-B resistance</td>
                <td>Parool Gupta</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/111.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWNiNDgyYWMtZGEzYS00YjI3LThkY2QtMjZkYTgzYzMxOTZm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>112</td>
                <td>"High glucose and advanced glycated end products induce <br>metabolic memory- like phenotype in human <br>endothelial cells through activation of<br> DNA methyltransferase 1"</td>
                <td>Sampara Vasishta</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/112.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjNkMDc2NDItY2NkNC00NDgyLTg1MDgtYzM3ZTI1M2E1NzNk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>113</td>
                <td>High glucose and lipopolysaccharides induce unique changes <br>in neutrophil phosphoproteome and activate <br>distinct signaling pathways</td>
                <td>Pooja Yedehalli Thimmappa</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/113.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGQxYjhmODktZmE4MS00YTQ5LWFlMzAtMmYwYjNmYmZiOWVm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>114</td>
                <td>Effect of cisplatin on renal iron homeostasis components:<br> implication in nephropathy</td>
                <td>Ayushi Aggarwal</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/114.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDAwYTRhNTQtZGEzYS00MjFiLTg1NTItNWE4OWViZWU5NDkz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>115</td>
                <td>A cell non-autonomous signalling of FOXO/DAF-16 ensures<br> germline quality control in response to somatic <br>DNA damage</td>
                <td>Gautam Chandra Sarkar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/115.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGEyNDEzMjgtMjRmMi00ZWVkLWEwZTItMTJlMTc0NmEwMWEy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>116</td>
                <td>Surface-associated and secretory proteome of Leishmania <br>donovani for epitope prediction to design vaccine<br> and serodiagnostic marker</td>
                <td>Munawwar Karim</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/116.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGIwY2VmNDEtMTRiMy00NjFiLTg1MjMtYmE0ZGZlNjEyMjcz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>117</td>
                <td>To investigate the role of HPIP phosphorylation<br> in cell cycle regulation</td>
                <td>Anita Kumari</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/117.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWY5ZjI4MzQtYjNmMy00OWMxLThlMzMtMjQ1ZTc2N2JiZDlm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>118</td>
                <td>Identification of novel Leishmania Iron Transporter<br> (LIT) and its potential role in Leishmania <br>parasites survival</td>
                <td>Priya Priydarshni</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/118.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjdkZjdjMTItZjI2My00MjU2LThlZDEtMGNkODg4YjdjZjgz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>119</td>
                <td>Characterization of SET domain protein SET2 in the <br>protozoan parasite Leishmania donovani</td>
                <td>Varshni Sharma</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/119.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmNhZmJlMjktYzg5YS00M2ZjLTkxOGEtN2QyYWIyM2FmYWNl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>120</td>
                <td>Structural &amp; functional insights into EhBAR domain <br>protein</td>
                <td>Devbrat Kumar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/120.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWIzMWRjZjAtNWE0Ny00NDZkLTg5ZmItYzBmNzViN2E0Yjg2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>121</td>
                <td>Palladium-Catalyzed Intermolecular Dehydrogenative <br>Carboamination of Alkenes with Amines <br>and N-Substituted Isatin</td>
                <td>Rajesh Kumar</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/121.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTUyZmE5ZTYtY2U4ZS00M2E4LThiMTctZTk4NWZjZmQ1M2Ji%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>122</td>
                <td>Homocysteine and its chemical modifications induce <br>bidirectional activation of neutrophils and <br>platelets during pathogenesis of stroke</td>
                <td>Aswathy S Nair</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/122.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjYyNzdjODctZjdiZS00MWZlLTkxMDgtOWNiYjcyM2MyYWQ5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>123</td>
                <td>Role of elongator protein Elp3a in the protozoan <br>parasite Leishmania donovani</td>
                <td>Arushi Khanna</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/123.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTFkNjc2YjgtNzk3YS00MDdkLWIzOTQtNzIzMTRlYTU4MjMy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>124</td>
                <td>Design, Synthesis, Characterization and Invitro IMR32 <br>Cell line Cytotoxic Study Of Some Novel <br>1,2,4- triazole Derivatives
                </td>
                <td>D S. Sathyanarayana</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/124.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzA4NzFiMzUtMmRhYy00ZjY4LWFlYmUtOGZmMTUyZmNlOTky%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>125</td>
                <td>Role of Mge1 (nucleotide exchange factor for mitochondrial<br> Hsp70) in Global stress response</td>
                <td>Arun kumar Parpati</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/125.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODI5MTA1Y2MtNThlZC00ZWEwLTkzYmMtYzViM2VkZWEwMzQy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>126</td>
                <td>In silico Analysis Of  Multiple Sericin Proteins</td>
                <td>ANAND SALVI</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/126.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzdiYWRhMjktOGRkZS00MjlhLWIwOGItZDhhOGFhYmMxZDBj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>127</td>
                <td>Untargeted metabolomics using hyphenated analytical <br>techniques to unfold the secondary metabolites of<br> Clerodendrum glandulosum Lindl. Leaves</td>
                <td>Prashanta Kumar Deb</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/127.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTY0M2YxN2UtMGM1Yi00MjQ3LTgwYTAtOTkxMjAyN2Y1NTI1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>128</td>
                <td>Activation of Toll/NF-κB pathway in Drosophila <br>hematopoietic niche mediates hematopoietic response<br> upon benzene exposure</td>
                <td>Leonard Clinton D’Souza</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/128.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWQ5M2RkOGItY2M5OC00ODgxLWI4NzgtZGE5YmRhMzkwNzU5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>129</td>
                <td>Prolonged sunlight (UV rays) exposure induces PKDL<br> pathogenesis mediated by Vitamin-D</td>
                <td>Sachidananda Behera</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/129.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Zjg4ZjQ5MTEtNTY1NC00MTQxLWJkZjktZWM1M2M2MmI4NzEw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>130</td>
                <td>Structural bioinformatics and immunoinformatic approach<br> to designing multiepitope vaccine candidates <br>against functional amyloids of Pseudomonas aeruginosa</td>
                <td>Ayesha Z. Beg</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/130.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmFlNzA5ODctMmY2Yi00YmM2LWFiYmYtY2M1ZTBkODlkYjQ0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>131</td>
                <td>Antimicrobial drug resistance from food sources in <br>India</td>
                <td>Ghouse Peer</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/131.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmE2MGNjYzUtZDAwYy00M2Y3LWI3NDQtMDJkM2Y1MTYyYWYy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>132</td>
                <td>Effect of Air Pollution on Plant Vegetation and<br> Agricultural Crops: A Review</td>
                <td>Dr. Meenakshi Nandal</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/132.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTIzYWRmNGEtZjJjMi00YWJkLTg0OGQtNjlmOTQzNDYwZjBj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>133</td>
                <td>"The Current Advances in Identification and <br>Characterization of PET Hydrolases for PET Bio<br> recycling"</td>
                <td>Rahul Saginela</td>         
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/133.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTcwMzZhZTgtMzU5MS00YTI4LWIwZTMtZjJhMzhmMGRkMWY3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>134</td>
                <td>Untargeted metabolomics using hyphenated <br>analytical techniques to unfold the secondary metabolites<br> of Clerodendrum glandulosum Lindl. Leaves</td>
                <td>Prashanta Kumar Deb</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/134.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjdhYjhmMWQtNmUzZC00NzQxLTgzNjAtZGRiYjkzNzU3OGFl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>135</td>
                <td>Novel Bacteriophages as an agent to control<br> Vibrio infection in aquaculture</td>
                <td>Sovon Acharya</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/135.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGU5NDFjMjctZmJhNi00YjQ2LWIzZTctZTM4ZWMyNDkzZjQ2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>136</td>
                <td>Immunomodulatory Effects of Fungal Cell Wall<br> Components</td>
                <td>Asha Rani</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/136.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OWRiZWEyZGMtZjg0OC00Y2I4LTljNGItOGIwZDVkMGU5ODcy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>137</td>
                <td>Analysis of major bacterial genera in diabetic <br>foot ulcers of different wound severities <br>based on Next Generation Sequencing</td>
                <td>Murali TS</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/137.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDRlNGM4ZGYtN2QwYS00YzQ4LTgzMjYtYzFkMTkyZWFiZDZj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>138</td>
                <td>Studies on paternal calorie restriction in obese <br>rats and their offspring</td>
                <td>Anuradha Rachakatla</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/138.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzQ1MmViMmItODkzMS00NzY3LWJjMjUtZDQ5ZDEzZjNjNzkx%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>139</td>
                <td>Lipin-1 is regulated by PLK1, AKT1, LKB1 and PP2A <br>and has a role in mitosis in mammalian <br>cells</td>
                <td>Misbah un Nisa</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/139.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODIyMDIyMzctYTA1NS00MGM4LThiOTctMmNkOTRiOTE0OTQ2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>140</td>
                <td>The Role of Autophagy in Renal Interstitial<br> Fibrosis</td>
                <td>Ruby</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/140.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MjE2ZmJmNTItY2Y5YS00ZjZjLWE3NGUtNmQyOGIxMmJiMmY0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>141</td>
                <td>Engineered improved siRNA therapeutics against <br>metastatic breast cancer</td>
                <td>Argha Mario Mallick</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/141.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTFkMDVhZDgtM2EwMS00MDcwLTg1NTgtNzhjYjM2OTU5NzA2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>142</td>
                <td>Role of Redox Proteins NOX-1 and Nrf-2 in Pro-survival<br> Autophagy in Cancer</td>
                <td>Bhargav N. Waghela</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/142.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MjkxOGU5MWEtNTI4Yy00Y2FkLTg5NmMtNmExY2FiNDFjZTMw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>143</td>
                <td>Do Peptidyl Prolyl isomerases modulate aggregate<br> formation in Amyotrophic lateral sclerosis?</td>
                <td>Lovleen Garg</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/143.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDc2YmFmOWQtMzBjMy00ZWM4LWI4MzYtMjI1NjE1MzRjYWVj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>144</td>
                <td>Methods to synthesized coumarin based dyes</td>
                <td>Kritika</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/144.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzZlOGI0ZjItZGM4Yi00NTU0LWIyYWMtODVjMzIyNDJlNjE0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>145</td>
                <td>Prolonged sunlight (UV rays) exposure induces PKDL<br> pathogenesis mediated by Vitamin-D</td>
                <td>Sachidananda Behera</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/145.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGI5ODk0NWUtYzFiNy00MWU5LTlhZDEtYTIzOTFiZjJkODNm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>146</td>
                <td>Micronized Silymarin suspension: Readily translatable<br> side-effect free polyphenol compound attenuates<br> hypobaric hypoxia induced oxidative stress</td>
                <td>Y Ahmad</td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/146.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NTBiNjUxZWUtOGUwZi00YjgyLTliYWUtNDY3Y2VmNDM0MWFk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>147</td>
                <td>Activation of Toll/NF-κB pathway in Drosophila <br>hematopoietic niche mediates hematopoietic <br>response upon benzene exposure</td>
                <td>Leonard Clinton D’Souza</td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/147.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTlmYTFjNjctYmM1Ny00YTJmLWEwNDctOWQ1ZGJiODg3YzI5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>148</td>
                <td></td>
                <td></td>         
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/148.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Y2UwYTc3NTAtNzgyNS00MTQwLWE3MWItMjdkMzc2ZWVlMjdj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>149</td>
                <td></td>
                <td></td>            
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/149.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjRmMWY1MDMtZDY3OC00NGMyLTg4MjUtY2JmOTBiMDA4YWE4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>150</td>
                <td></td>
                <td></td>
                <td>19th December</td>
                <td>2:15 PM - 3:45 PM</td>
                <td><a href="assets/img/Poster Day Wise/150.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWY5N2Y2YTAtN2ZhMy00NGFkLWE1YjgtYmVmMzc1ZDNlZGVk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22a5352dc3-b9b5-4fd0-9fff-e3fa76408817%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
         
        </tbody>
        <!-- <tfoot>
            <tr>
                <th>Name</th>
                <th>Breakout Room</th>
                
            </tr>
        </tfoot> -->
    </table>
        </div>


        
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>


<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>

<script>
   $(document).ready(function() {
    $('#example').DataTable( {
        "scrollY": 500,
        paging: false,
        show: false,
        entries:false,
        "scrollX": true
    } );
    $("tbody td").css("border","1px solid #000");
} );
</script>
</body>