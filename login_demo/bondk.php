<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = '913234bfe40b6433e81ceb7573bdd9b9c069ad2c08d89d3beb33bdc351ed5954';
require_once "exhibcheck.php";
$curr_room = 'bondk';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<style>
table td, table tr, table th{
background: transparent !important;
}

.txtcolor{
    color:black;
}

table.dataTable thead .sorting_asc {
    background-image: url("") !important;
}

.backbtn{
    float:left;
   background-color: black;
   padding:3px;
}
#example{
    display :none;
}

.h2{
    color:black
}
/* .border{
    border-color:black;
} */
.bg-image{
    /* background-image: url('assets/img/listbg.jpg');
    background-size: cover;   */
    background-color:default;
    font-size: 15px;
    
}
.mfp-iframe-holder .mfp-content {
    line-height: 0;
    width: 100%;
    max-width: 1100px;
}
tr:nth-child(even) {
            background-color: lightgrey !important;
        };
th{
    text-align: left;
}
</style>
<body>
<div class="page-content">
    <div id="content1">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
       
        <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white">
        <div id="back-button" class="backbtn">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
        </div>
        <h1 class="mr-4 text-dark">POSTER LISTS</h1>
        <a href="#example" class="btn btn-info" role="button" aria-pressed="true">18.12.2021</a>
        <a href="bondk2.php" class="btn btn-info" role="button" aria-pressed="true">19.12.2021</a>
        <table id="example" class="table" style="width:100%;">
        <thead>
            <tr>
                <th>S No.</th>
                <th>Subject</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Poster</th>
                <th>Breakout Room</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Valorization of rice agro-residues under the <br>influence of lignocellulolytic enzymes <br>to release phenolic compounds and<br> their antioxidant properties</td>
                <td>RV Beladhadi</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/1.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTFkYmUwYWItOTBhNS00ZGJmLWIwNzMtNWM1MDgxZjAzMDYz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Production of xylitol by the fermentation<br> of the hemicellulosic hydrolysates of<br> banana and water hyacinth leaves by adapted and <br>un-adapted yeast</td>
                <td>Kumar Shankar</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/2.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDM2NTY5ODgtZWFjMi00ZWVhLWFiYTEtYTAzYTQxY2EzMDk0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Cysteine-rich secretory proteins involved <br>in Bacteroid differentiation <br>of Peanut root nodules via their<br> putative lipid binding activity</td>
                <td>Bikash Raul</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/3.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGI1Y2UzNTMtNTFjNS00ODAwLWIxNDQtY2E4OGY3YWU0OTdm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>4</td>
                <td>The prospective antiglycation activity of Syzygium <br>jambolanum for diabetic nephropathy</td>
                <td>Nilima Bangar</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/4.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGRkYWZmODktNGQ5My00ZmNiLTgxYmUtMTNiMWQyNTUxYWI4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Production of acetone, butanol and ethanol <br>from enzymatic hydrolysates of <br>steam pretreated pineapple leaves <br>and banana leaves</td>
                <td>Rakesh Sajjanshetty</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/5.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_M2I3ZmU2YTYtMWU5NC00ZjA4LWJmMjMtODRhZjRhN2QyNjdk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>6</td>
                <td>Untargeted Metabolite Fingerprinting Reveals <br>Impact of Geographical and Species Origin<br> on Bioactive Components of  Tinospora Species Stem</td>
                <td>Chigateri M Vinay</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/6.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzhlNTczOTMtYjc5NS00MTkwLTgwZWQtOTBmOGVjMzMzOTA4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>7</td>
                <td>Silica coated porous zinc oxide nanocomposite: <br>A potent nanocarrier for controlled <br>release of tricyclazole</td>
                <td>Annu Yadav</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/7.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTYwYjQwN2MtODAxYy00OGRhLWJkNGItYzBjMTVmYjkyM2Mx%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>8</td>
                <td>Exploring the NIN independent early signalling <br>and associated cell wall modifications<br> during crack entry in peanut</td>
                <td>Oindrila Bhattacharjee</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/8.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTE5YTRlMTEtMzA5OS00YzA5LWIzOTEtNmMzZjAzYTRkMWRl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>9</td>
                <td>Experimental validation of pre-identified candidate<br> genes with probable role in protein storage<br> inside chickpea (Cicer arietinum) seeds</td>
                <td>Prashant Yadav</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/9.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTI1MWJlODAtNTc1MS00NjE4LTgzZDQtYzk0YTdkZGE2ZDg2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>10</td>
                <td>Development of essential oils loaded polymeric<br> nano formulations and its <br>characterization</td>
                <td>Raj Kumar Salar</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/10.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDg0NjZiMGUtZDg0OS00MjJlLTk5MzQtZjIwYTQ1NmRjNGYx%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>11</td>
                <td>Silver Quantum Dots Using Extract Of Plant <br>Abies Webbiana.</td>
                <td>Navjot Mehta</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/11.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODM2Nzc3OTYtYjMwMC00YWQyLWJjYmEtNGI2ZGJiYjQ0OGJi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>12</td>
                <td>Anti-Obesity Activity of Annona Squamosa  <br>(Linn.) Leaves In Monosodium Glutamate - <br>High Fat Diet Induced Obese Mice</td>
                <td>Ravi Pratap Singh</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/12.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTcwNmE2N2EtNzg2OS00NjdhLTg5MGEtYmJlMDk4ZDg3MTJh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>13</td>
                <td>Capsaicin: A molecule with potential  <br>health benefits</td>
                <td>Pardeep Kumar</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/13.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTc3YjEyOTgtMjU2OC00MGI2LTllMTYtYzY3OGE5Zjk1MmZi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>14</td>
                <td>An application of a Bio-control agent in <br>management of an obnoxiousweed  <br>-Parthenium hysterophorus L.</td>
                <td>Narendra Kumar</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/14.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YmViMzY3M2YtNzY5NC00ZDYzLWJjZTktOTc3ODEwZmQ1MDNm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>15</td>
                <td>Effect of Noble Metal Nanoparticle on <br>Photosynthesis Reaction</td>
                <td>Shikha Dhiman</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/15.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_M2ViMzNmMTctYTgwNC00MTkzLWI1MWEtMjRlMzU3MjAxMTZl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>16</td>
                <td>Solvent optimization for isolation of anthocyanin  <br>rich extracts of Ocimum and Hibiscus species: <br>focused on antioxidant and  antimicrobial mechanism</td>
                <td>Amrita Chatterjee</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/16.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmUwNTkwMTUtZTQwZC00MjMwLTlhNjEtMTA3OTdkMDY0NDA2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>17</td>
                <td>Understanding The Transcriptional Regulation<br> During The Symbiosome Development Of Medicago<br> truncatula Nodules</td>
                <td>Akanksha Bhardwaj</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/17.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGE2NmQxNzItZTc0Yi00YTExLWE3M2MtODNmNzA3MTRjNjAy%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>18</td>
                <td>Transcription factors duo in Medicago peripheral<br> vasculature organisation and nutrient<br> transportation</td>
                <td>Deevita Srivastava</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/18.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTE2YTI5ZjktY2IwNC00ZjI0LTgxNDEtMGU3MzJjODVlM2Yw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>19</td>
                <td>Novel insights into the Thymidylate kinase (TMK)<br> of the photosynthetic cyanobacterium <br>Nostoc sp. strain PCC7120</td>
                <td>Anurag Kirti</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/19.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Nzk3ZjI0MGMtYWY3ZS00OTNkLTlkZGYtYjk1MTA4ZTQyYmVj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>20</td>
                <td>Spermidine Supplementation Relieves Thermal<br> Stress of 5th Instar Silkworm, Bombyx mori</td>
                <td>Anugata Lima</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/20.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGQxOTEyZDgtOGU2YS00ZjI4LWFkZmUtN2ZkYzg2MzdmNDNj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>21</td>
                <td>Functional characterization of Hybrid Histidine<br> Kinase III from pathogenic fungi causing Mucormycosis.</td>
                <td>Anita Yadav</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/21.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjVhZTkzNDUtMWQyMC00ZDhhLWIwYzYtY2U2ZTU5YzhhOWZm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>22</td>
                <td>Production of biofuel precurors in e. coli</td>
                <td>Kamini Singh</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/22.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTg1NTBjNTUtNjM2MS00ZjU5LWExYjUtM2EyNDAzY2E5YjAw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>23</td>
                <td>Magnesium influences Candida albicans counter <br>strategies for immun recognition by alteringe<br> hyphal damage, β-glucan <br>masking and vacuole homeostasis</td>
                <td>Sandeep Hans</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/23.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjEyYjc5OTktODg2My00Y2VlLWE2MGQtOWQ4MmM5ZTAxNDc5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>24</td>
                <td>Differential effect of Polyols on the refolding <br>of Microbial α-amylases</td>
                <td>Aziz Ahmad</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/24.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWNlZTZkYzItNGUxYy00OTlkLWJhZTQtNDgwYThmNDBiZDBm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>25</td>
                <td>Functional characterization of CuNik1, a hybrid <br>histidine kinase from<br> emerging fungal pathogen Candida auris</td>
                <td>Yogita Martoliya</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/25.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_N2U2YzY1MDEtZTNlOC00NmJjLTljNzEtNzBkNGE4ZjZmNzc3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>26</td>
                <td>SARS CoV-2 UTRs interacts with host RBPs and<br>modulates pre-mRNA splicing</td>
                <td>Kush Kumar Pandey</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/26.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGE5Nzc1NmUtZGI3OS00ODU0LThhYzEtNzQ0N2MyY2RhYWQ5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>27</td>
                <td>Enhancing the anti-TB immune response by blocking <br>TLR2-PPE18 interaction<br> with small molecule inhibitors</td>
                <td>Brahmaji Sontyana</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/27.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWE0NTJkYTctYTM2Yi00MGNhLTkwNzktOThkOWRhNTBmZDQz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>28</td>
                <td>Quantitative detection of a cocktail of Mycobacterium<br> tuberculosis MPT-64 (Rv1980c) and ESAT-6 (Rv3875) in <br>urogenital tuberculosis patients by real-time <br>immuno-PCR: comparison with GeneXpert MTB/RIF assay</td>
                <td>Ekta Kamra</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/28.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmNmOTg5YmUtOGUzMC00YzZkLTljODYtZmM2YTRjNThlNTk0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>29</td>
                <td>Elucidating the involvement of heme biosynthesis<br> and hemoglobin uptake in the<br> development of artemisinin resistance<br> in Plasmodium falciparum parasites</td>
                <td>Abdur Rahman</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/29.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWJjNmNmNjAtOTUzMS00NGU3LWIzNzQtNjI3N2EyMjcyYTFm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>30</td>
                <td>PfMYST/RUVBL complex plays role in immune <br>evasion of malaria parasite by <br>regulating var genes in sub telomeric region</td>
                <td>Himani Saxena</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/30.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWNkODhlOWQtNWQzMS00NmM5LWE0ODEtNGQzNTRjODk1MTBl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>31</td>
                <td>Biodegradation (Composting) of Poultry <br>feathers using Keratinolytic Bacteria</td>
                <td>Priyanka Bumbra</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/31.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWZlMWVkYWEtNjRjNi00MzM1LTkyZTAtZjhjMmY5MzViZDEw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>32</td>
                <td>A targeted sequencing-based approach for<br> the detection of drug-resistant <br>tuberculosis </td>
                <td>Nikhil Bhalla</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/32.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MjYwMTllMGUtMzE4Yi00M2I0LWI2NTktNWZjMmI0MjM3OGE4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>33</td>
                <td>Targeting cell wall protein, LipX, as a<br> potential target for anti-TB drug therapeutics</td>
                <td>Priyanka Dahiya</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/33.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzU1NmFhY2YtNzBlYS00MWRjLThjYjktY2M3NjEwMWNjYWJi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>34</td>
                <td>Computational analysis identified <br>mRNA-based therapeutic agents against <br>zoonotic bovine tuberculosis</td>
                <td>Ayan Mukherjee</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/34.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OWY5NzRiNTYtZTAzMy00NmRjLWE1YWEtYmI3ZTY1MGYwNTZj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>35</td>
                <td>Genome-wide analysis reveals two distinct<br> classes of PTR/POT transporters <br>in Candida species</td>
                <td>Rosy Khatoon</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/35.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MzdkYmU1ZGEtOTQyOC00MzAyLWEwZTUtZTMwMDcxODY4Mzk3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>36</td>
                <td>Structural, Phylogenetic and Drug target <br>analysis of putrescine monooxygenase<br> from Shewanella putrefaciens-95 using<br> Biowarningrmatic tools</td>
                <td>Saroja Narsing Rao</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/36.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MzM3Y2Q5ZmMtMmZlMC00NDllLTlhZjQtNjIwZDg2NmI5MDI0%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>37</td>
                <td>Effect of Trans Ferulic Acid Loaded Chitosan<br> Nanoparticles on Growth of Various<br> Gram Positive and Gram Negative Bacteria</td>
                <td>Usha Rani</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/37.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWZmN2RmNjYtOGRiZS00OTZjLWJmZTQtY2M5MmE4N2NiNGMw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>38</td>
                <td>An insight into the chromatin binding sites<br> of the human malaria parasite Plasmodium<br> falciparum MCM6</td>
                <td>Shashank Shekhar</td>            
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/38.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGZlMzgzZmQtNzVmOC00N2RlLThkYzAtMGU4NzIyMGVkNzE1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>39</td>
                <td>Role of Immuno-PCR in Tuberculous Meningitis<br> Diagnosis</td>
                <td>Sonia Ahlawat</td>
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/39.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDY1MmMzMWQtZDIyOC00ZWM5LTlhNzQtYTE1MTAxNzI4Y2Nm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>40</td>
                <td>Investigative Study on Seizing the Cell Cycle for <br>Bacterial Colonization<br> on Skin Using Natural Antibiotics</td>
                <td>Samhita Ramamoorthy</td>         
                <td>18th December</td>
                <td>2 PM - 3:30 PM</td>
                <td><a href="assets/img/Poster Day Wise/40.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZmY2OGRlNzQtOWU0Ny00N2M2LTgwNTgtMjM5MmM0NzAwMDkx%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>41</td>
                <td>Anti-fungal activity of Silver Nanoaparticles <br>derived from Mesua ferrea <br>seed extract</td>
                <td>Manju Sangwan</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/41.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmI1YTc0ODgtNGUwOC00YzNlLWEyMjEtNTY3ZDc1NzU1MDli%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>42</td>
                <td>ESAT-6 protein of Mycobacterium tuberculosis <br>inhibits the differentiation <br>of human monocytes to dendritic cells</td>
                <td>Akshay G M</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/42.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjkxMzM2OWEtMTEzOS00YTM3LWEzODEtNzgxOTMwYjEzMDFj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>43</td>
                <td>Antimicrobial peptides: A novel approach of <br>drug design</td>
                <td>Ravneet Chug </td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/43.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OWQwODg5ZTEtZWQyYS00ZTk2LWExNzEtNTY3NjIxZDhkODY1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>44</td>
                <td>Insights into the mechanism of directed<br> evolution of drug resistance in <br>Candida auris.</td>
                <td>Praveen Kumar</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/44.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTUzOGU3ZTAtODQxYy00ZTg2LWJkNWYtMzQ2ZTY3YTgzYjhj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>45</td>
                <td>Structural and functional characterization<br> of EccCb1 of M.tuberculosis ESX-1<br> system</td>
                <td>Arkita Bandyopadhyay</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/45.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODFmMzMzZDItMDZiZC00Y2MwLTg0NzktNGNmN2IzOWY4ZTY1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>46</td>
                <td>Studies on characterization of novel <br>mycobacteriophage proteins with<br> mycobactericidal properties</td>
                <td>Saddam Ansari </td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/46.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjAyMGJkZTgtYmUyNy00YmYwLTk5MGItMzBkMmFiM2M5NjVl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>47</td>
                <td>Structure and mechanism of key enzymes <br>involved in GDPHeptose biosynthetic pathway<br> in M. tuberculosis: potential drug target</td>
                <td>Ramesh Kumar</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/47.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTdhMWY5ZDItZWY2MS00NDY3LTkyNmItMGZjODRiYTBlOTRh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>48</td>
                <td>Developing a CRISPR/Cas9 system for genome <br>editing in halotolerant yeast <br>Debaryomyces hansenii</td>
                <td>Mohd. Wasi </td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/48.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmYyZDJlNzctMDY5ZS00NGY1LWFhOGYtNDJjODY1NzM5NWFi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>49</td>
                <td>Evolving recombinant Saccharomyces cerevisiae <br>for enhanced xylose uptake</td>
                <td>Dhwani Gupta</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/49.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTAxN2VhNDYtMzQyNC00YTUzLTg0MGUtZGRiMjVmYzgwYjJi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>50</td>
                <td>Pimozide eradicates S. aureus infection in C.elegans</td>
                <td>Siddhartha Kumar</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/50.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzZmZTAwNzAtYjg5NS00N2Y5LTk5NjItODNiNDM1NjQwMmEw%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22793b610c-2932-408a-a79e-a7f4e4ff949b%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>51</td>
                <td>Exploring antibiofilm potential of bacitracin<br> against streptococcus <br>mutans</td>
                <td>Sahar Zaidi</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/51.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmI0ODhhNmMtYTg5Zi00ZWM0LTk3ZGEtOGRlMjI0MmUwNTBi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>52</td>
                <td>Cellular Prion protein regulates vascular <br>smooth muscle cell migration <br>at the maternal fetal interface</td>
                <td>Rumela Bose</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/52.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NDQ0YzllZjEtMDlmMC00YTg3LWI2YTctNTk0MDUxZDkwNDA5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>53</td>
                <td>Bisphenol A in Adult Rats Alters Endocrine<br> and Reproductive Factors Resembling<br> the Polycystic Ovarian Syndrome Phenotype</td>
                <td>Navya Prabhu B</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/53.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzJjMGViMjYtOGUyZS00Yzg3LWE1MmYtZTc1MWQzNjQ4MWI2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>54</td>
                <td>PHLPP1 promotes lipid accumulation in foam cells, <br>zebrafish larvae and C. elegans <br>through AMPK/ChREBP dependent pathways</td>
                <td>Keerthana</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/54.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OTUzMWNiYzYtZTIwZC00YTliLTliOGMtMTY1NGFjNjRiZGNj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>55</td>
                <td>Role of Eisosome Proteins in Mitophagy & Cell death</td>
                <td>Amita Pal</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/55.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDRjMDAxM2MtYTY4Ny00MjM3LWJjZGYtMDE1MTZjMjEzZWM1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>56</td>
                <td>Gemini Lipid Nanoparticle (GLNP)-mediated Oral <br>Delivery of TNF- ⍺ siRNA Mitigates Gut<br> Inflammation via Inhibiting the Infiltration<br> and Differentiation of CD4 + T Cells</td>
                <td>Priyanka Verma</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/56.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTYzNDk2NTktNWY3MS00ODY5LWIwMzEtNWZlN2U3YjExZTc2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>57</td>
                <td>Role of MIA40 in PINK1 import, stability and <br>mitophagy</td>
                <td>Vandana Bisoyi</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/57.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTc0YTlkYmUtNzA5NS00MzI3LWJmNDYtODNkYzFiZWVmOTE1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>58</td>
                <td>Adaptive capacity to dietary Vitamin B12 levels<br> is maintained by a gene-diet <br>interaction that ensures optimal life span</td>
                <td>Tripti Nair</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/58.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_Yjg2MmNjYzItYThlMS00Y2UxLTk1NWEtOTM2YWI1NzEyOWE3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>59</td>
                <td>An RNA G-quadruplex in the 5’UTR of human <br>cIAP1 mRNA promotes translation<br> in living cells</td>
                <td>Aditya Singha Roy</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/59.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MGQ1OWU3OTYtZjBmZC00NGUxLThiMjUtNjdkMWE3ZjU2YzY1%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>60</td>
                <td>G1 cyclins regulate oocyte quality and are <br>essential for fertility<br> under low insulin signaling</td>
                <td>Umanshi Rautela</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/60.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGQ0OTcxN2ItMzQ5Ny00OTFhLTg2MWQtMzY1MmUzYmQzYjFh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>61</td>
                <td>Phosphoproteomics for understanding a protein-kinase<br> mediated signalling pathways  <br>in regulation of health and longevity</td>
                <td>Sabnam Sahin Rahman</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/61.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzEwMjJjOGYtYTdhOS00OWM0LTk1NGEtMWY5Y2I5NGM3M2Uz%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>62</td>
                <td>Identification of small molecule inhibitors  <br>against MMP-9 intended for <br>cancer therapy</td>
                <td>Khushboo Sinha</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/62.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_N2QxM2RmOWMtYzczNC00MzZlLTkzNWYtZTQxOWQ3NjQxMjNl%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>63</td>
                <td>Rationally designed peptide-based cancer  <br>nanotherapeutics</td>
                <td>Kasturee</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/63.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MjhhZGJjNGYtMTc5OS00ZjZjLTg0MDktY2M2OWU5OGNkOTBm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>64</td>
                <td>A Localized Docetaxel-Carboplatin Hydrogel  <br>Therapy Causes Apoptotic and <br>Immunogenic Cell Death via Activation of Ceramide-mediated<br> Unfolded Protein Response</td>
                <td>Animesh Kar</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/64.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDUzZDI5ZmUtYWY3MC00MGZhLWEwOTAtZDBmNTM5M2IyNTky%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>65</td>
                <td>Alternative splicing of Ceramide Synthase 2  <br>alters levels of specific <br>ceramides and modulates cancer cell proliferation and <br>migration in Luminal B breast cancer subtype</td>
                <td>Trishna Pani</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/65.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YThhYzQwNTgtNWVkMy00YzE3LWIzOTYtYzA1ZmVlOWJmOGFm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>66</td>
                <td>Transcriptome analyses reveal novel gene fusion  <br>events in Early-Onset <br>Sporadic Rectal Cancer</td>
                <td>Asmita Gupta</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/66.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzQyZTM0NzYtNmFmZS00MzM2LWE2NTItZmEzMThkMTRlOTZj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>67</td>
                <td>Transforming growth factor-beta (TGF-β) regulated  <br>lncRNA-MUF promotes <br>invasion by targeting miR-34a- Snail1 <br>axis in Glioblastoma multiforme (GBM)</td>
                <td>Bakhya Shree</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/67.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGM4MDZkMWItOGM4MC00ZmE0LWE5ZmYtNmIzNzI4MWNjMThh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>68</td>
                <td>Synthetic BMP2 secretagogue prevents glucocorticoid<br> induced osteopenia by activating osteogenic <br>WNT/β-catenin signaling and promoting osteoblast <br>survival by reducing ROS production</td>
                <td>Divya Rai</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/68.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWU1M2I4YmYtZjJjNS00OTAyLWIxNzEtNjFkY2IxNjliMWZi%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>69</td>
                <td>Star-Pap Control Of Microrna Expression Regulates<br> Oncogene Expression And Tumour Formation<br> In Breast Cancer</td>
                <td>Neeraja K M</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/69.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZmE0YmMyNDgtMjRjMy00ZjYyLWJhZTYtNzhhZWIxNTMyZGI5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>70</td>
                <td>PCTAIRE1: a novel regulator of mitosis and its <br>relevance to cancer</td>
                <td>Nusrat Nabi</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/70.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWM0YjUyMGItYmNlMS00NmM2LTllZDEtZjJkMmE0ZjdmZTQ3%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22efe0c9cc-f258-4597-ad55-1ce67de09cc8%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>71</td>
                <td>Identification of novel oncogenic targets of<br> mutant p53 in esophageal<br> squamous cell carcinoma</td>
                <td>Sara A George</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/71.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTlhMjM5ODQtYjYxNi00MGExLWFmYTctYjRhZTczMWE4OTYx%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>72</td>
                <td>Role of m6A RNA methylation regulated miRNAs<br> in oral squamous cell <br>carcinoma (OSCC)</td>
                <td>Jayasree PJ</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/72.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDczNDAwM2ItMGUwYy00OTNlLTk1YmUtYTgwOTc1ZjFkOWZj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>73</td>
                <td>Tp53 regulated neural differentiation factor,<br> Gene-X, plays a tumor <br>suppressive role in glioblastoma</td>
                <td>Anirban Jana</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/73.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzBkN2UyYjgtM2UxNi00YTE3LWEzNTAtMDRiZmIxMDRmOWJj%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>74</td>
                <td>miR-210: An attractive target for therapy of <br>Glioblastoma</td>
                <td>Indranil Mondal</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/74.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NTg2MjY2NmEtYzVmNS00YmU0LTlhMjItODhkOTdlZTU0YTJh%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>75</td>
                <td>Investigating the role of hypoxia regulated<br> long non-coding rnas in glioblastoma</td>
                <td>Garima Yadav</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/75.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MjlhNjc5OWUtNTQwYi00ZGJlLWJlNzQtZDExNjNiZTkzNmQ4%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>76</td>
                <td>Regulation of Mitochondrial Structure <br>and Functions by Bisphenol A <br>in Cervical Cancer</td>
                <td>Nadeem Khan G</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/76.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjgxZTQwYTMtYzhjMi00ZGMwLWE5N2ItYmJiMTk3Nzg1NDc2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>77</td>
                <td>Exosome Mediated Transfer of  Double C2 <br>Like Domain Containing Protein<br> Beta (DOC2B) Shows Tumor Growth<br> Regulatory Functions <br>in Cervical Cancer </td>
                <td>Sangavi Eswaran</td>            
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/77.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDlmMTQ2YzAtYmEzZi00MjM0LTg4MzgtYmYzODFlNGExNGI2%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                <td>78</td>
                <td>miR-379/656 Cluster Functions as a <br>Tumor Suppressor by Targeting PDK3<br> in Cervical Cancer </td>
                <td>Sriharikrishnaa S</td>
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/78.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YTQ3YzdkOGItMmU0ZC00MzU5LWEzZDUtMTNhNTZiYjQ0NTlm%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
            <tr>
                
                <td>79</td>
                <td>The association of drug metabolizing<br> enzymes CYPs among HNSCC patients<br> and their treatment outcome</td>
                <td>Vinay Kumar Yadav</td>         
                <td>18th December</td>
                <td>3:30 PM - 5 PM</td>
                <td><a href="assets/img/Poster Day Wise/79.pdf" class="showpdf btn btn-warning" role="button" target="_blank" aria-pressed="true">View Poster</a></td>
                <td><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmJkMzRhODItOTIyZC00YTQ1LTk5ZWYtYzVhMDhjMmY5YTNk%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%22ee69ae29-6b8c-4b62-b8e4-254f798658ed%22%7d" class="btn btn-primary" role="button" target="_blank" aria-pressed="true">Talk to the Presenter</a></td>
            </tr>
         
        </tbody>
        
    </table>
        </div>


        
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>


<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>

<script>
   $(document).ready(function() {
    $('#example').DataTable( {
       // "scrollY": 500,
        paging: false,
        show: false,
        entries:false,
        "scrollX": true,
        "info": false
    } );
    $("tbody td").css("border","1px solid #000");
} );
</script>
</body>