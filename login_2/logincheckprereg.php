<?php
require_once "functions.php";

$userid = '';
$user_name = '';
$user_email = '';
$cert='';
$loginUrl = './';
//echo $_SESSION['userid'];
//echo $_SESSION['emailid'];
if (!isset($_SESSION['userid'])) {
    header("location: " . $loginUrl);
} else {
    $userid = $_SESSION['userid'];
    $emailid = $_SESSION['emailid'];
    $member = new Preregusers();
    $member->__set('emailid', $emailid);
    $user = $member->getUser();
    //var_dump($user);
    if (!empty($user)) {
        $user_name = $user[0]['name'];
        $user_email = $user[0]['emailid'];
        $user_affiliation=$user[0]['affiliation'];
        if($user[0]['type']==1){
        //echo "is a Speaker"; 
        $cert='assets/img/Certificate_Speaker.jpg';
        }
        if($user[0]['type']==2){
          //  echo "is a Presentor"; 
        $cert='assets/img/Certificate_Presentor.jpg';
        }
        if($user[0]['type']==3){
           // echo "is a Participant"; 
        $cert='assets/img/Certificate_Participant.jpg';
        }
    } else {
        header("location: " . $loginUrl);
    }
}
