<?php
require_once "logincheck.php";
$curr_room = 'lobby';
// require_once "enter.php";
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Amity Lobby  Recoveredwith Poster F1 email.jpg">
            <a href="3. Video to play in lobby.mp4" id="lobbyVideo" class="viewvideo" >
                    <div class="indicator d-6"></div>
                <!-- <iframe src="https://www.youtube.com/embed/KtV0OgQWVzs" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        -->
                </a>
            <a href="auditorium1.php" id="enterAudi1" >
                <div class="indicator d-6"></div>
            </a>
            <a href="auditorium2.php" id="enterAudi2">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Hanging banner left front 1/Dr. Ashok K Chauhan (Founder President) VER 1.jpg" id="hangingphoto1" class="view hangingphoto1" data-vidid="11">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Recovered Poster -1 Enhanced.jpg" id="poster" class="view poster" data-vidid="12">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Hanging banner left back/Dr. Aseem Chauhan (Chancellor Amity University Haryana) VER 1.jpg" id="hangingphoto2" class="view hangingphoto2" data-vidid="13">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Hanging banner right back/SBC(I) logo 1 (1) (1).png" id="hangingphoto3" class="view hangingphoto3" data-vidid="14">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Hanging banner right front/AUH Logo 1.jpg" id="hangingphoto4" class="view hangingphoto4" data-vidid="15">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Standee banner left front/GHI_12x18_220721021(1).jpg" id="standeebanner1" class="view standeebanner1" data-vidid="16">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Standee banner left back/Stroke_12x18_220721021(1).jpg" id="standeebanner2" class="view standeebanner2" data-vidid="17">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Standee banner right back/CVD_12x18_220721021(1).jpg" id="standeebanner3" class="view standeebanner3" data-vidid="18">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Standee banner right front/Stroke_12x18_220721021(1).jpg" id="standeebanner4" class="view standeebanner4" data-vidid="19">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="games.php" id="engagement">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="lounge.php" id="enterHall">
                <div class="indicator d-6"></div>
            </a>
           <a href="exhibitionhalls.php" id="enterLounge">
                <div class="indicator d-6"></div>
            </a>
             <a href="photobooth.php" id="photobooth">
                <div class="indicator d-6"></div>
            </a>
         
            <a href="assets/resources/90thSBCI full program.pdf" class="showpdf" id="showAgenda">
                <div class="indicator d-4"></div>
            </a>
             <!--   <a href="timeline.php" id="timeline">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="resource">
                <div class="indicator d-4"></div>
            </a> -->
             <!-- <a href="#" onclick="javascript:alert('Certificate will be available for download on 9th May 2021');" id="digitalcertificate">  -->
            <a href="digital_cert.php" id="digitalcertificate" class="digitalcertificate" data-vidid="20">
                <div class="indicator d-4"></div>
            </a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<section class="videotoplay" id="gotoaudi1" style="display:none;">
    <video class="videoplayer" id="gotoaudi1video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium1.php" class="skip">SKIP</a>
</section>
<section class="videotoplay" id="gotoaudi2" style="display:none;">
    <video class="videoplayer" id="gotoaudi2video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium2.php" class="skip">SKIP</a>
</section>
<section class="videotoplay" id="gotoaudi3" style="display:none;">
    <video class="videoplayer" id="gotoaudi3video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium3.php" class="skip">SKIP</a>
</section>
<?php require_once "scripts.php" ?>
<script>
    var audi1Video = document.getElementById("gotoaudi1video");
    audi1Video.addEventListener('ended', audi1End, false);
    var audi2Video = document.getElementById("gotoaudi2video");
    audi2Video.addEventListener('ended', audi2End, false);
    var audi3Video = document.getElementById("gotoaudi3video");
    audi3Video.addEventListener('ended', audi3End, false);

    function enterAudi1() {
        $('#content').css('display', 'none');
        $('#gotoaudi1').css('display', 'block');
        audi1Video.currentTime = 0;
        audi1Video.play();
    }

    function audi1End(e) {
        $('#gotoaudi1').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium1.php";
        }, 1000);

    }

    function enterAudi2() {
        $('#content').css('display', 'none');
        $('#gotoaudi2').css('display', 'block');
        audi2Video.currentTime = 0;
        audi2Video.play();
    }

    function audi2End(e) {
        $('#gotoaudi2').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium2.php";
        }, 1000);
    }

    function enterAudi3() {
        $('#content').css('display', 'none');
        $('#gotoaudi3').css('display', 'block');
        audi3Video.currentTime = 0;
        audi3Video.play();
    }

    function audi3End(e) {
        $('#gotoaudi3').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium3.php";
        }, 1000);
    }

    $(function() {
        /* $.magnificPopup.open({
            items: {
                src: 'https://player.vimeo.com/video/543708869'
            },
            type: 'iframe',
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>' +
                    '<div class="mfp-title"></div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'
        }); */
        $(document).on('click', '#resource', function() {
            //alert();
            $('#resourcesList').modal('show');
        });
    });


    $(function() {
        $('.hangingphoto1').on('click', function() {
            var vid_id = $(this).data('vidid');
           
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });

    $(function() {
        $('.poster').on('click', function() {
            var vid_id = $(this).data('vidid');
           
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.hangingphoto2').on('click', function() {
            var vid_id = $(this).data('vidid');
           
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.hangingphoto3').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.hangingphoto4').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.standeebanner1').on('click', function() {
            var vid_id = $(this).data('vidid');
           
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.standeebanner2').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.standeebanner3').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.standeebanner4').on('click', function() {
            var vid_id = $(this).data('vidid');
           
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.digitalcertificate').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            console.log("inside analytics function in js for digital certificate");
            var userid="<?php echo $_SESSION['userid']; ?>"
            var emailid="<?php echo $_SESSION['emailid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

             $.ajax({
                url: 'prereguserslogin.php',
                data: {
                    emailid:emailid 
                },

                type: 'post',
                success: function(response) {
                    if(response.length > 0){
                        console.log(response.length);
                        console.log(response);
                        var url= "digital_cert.php";
                        window.location = url;
                        return true;
                    }
                    else{
                        console.log(response.length);
                        console.log(response);
                        alert("Certificate available for Paid Users Only!");
                        return false;
                    }
                }
            });
                   return false;
        });
    });
</script>

<div class="modal fade" id="resourcesList" tabindex="-1" role="dialog" aria-labelledby="resourcesListTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="resourcesListLongTitle">Resources</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content scroll">

                    <ul class="popuplist">
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/profiles.pdf" class="viewpoppdf resdl">International Faculty Profiles</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/track1.pdf" class="viewpoppdf resdl">Track-1 UPPER EXTREMITY TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i>
                             <a href="assets/resources/conf/track2.pdf" class="viewpoppdf resdl">Track-2 FOOT ANKLE TRAUMA </a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/track3a.pdf" class="viewpoppdf resdl">Track-3 PATHOLOGICAL FRACTURES</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/track3b.pdf" class="viewpoppdf resdl">Track-3 SPINE TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/track4.pdf" class="viewpoppdf resdl">Track-4 KNEE TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> 
                            <a href="assets/resources/conf/track5.pdf" class="viewpoppdf resdl">Track-5 HIP PELVIS TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i>
                             <a href="assets/resources/conf/track6.pdf" class="viewpoppdf resdl">Track-6 INFECTION MANAGEMENT TRAUMA</a>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>